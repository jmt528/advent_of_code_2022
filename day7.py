#!/usr/bin/env python3
import input_data
data = input_data.day7_data
commands = [d.strip().strip('$ ').splitlines() for d in data.split('$ cd')]
commands.pop(0)

class Node():

    def __init__(self, name, value) -> None:
        if value == 'dir':
            self.type = 'dir'
            self.size = 0
        else:
            self.type = 'file'
            self.size = int(value)
        self.name = name
        self.previous = None
        self.next_nodes = []
        
    def add_node(self, name, value):
        new_node = Node(name, value)
        new_node.previous = self
        self.next_nodes.append(new_node)
        if value != 'dir':
            self.update_sizes(value)
    
    def update_sizes(self, value):
        self.size += int(value)
        if self.previous is not None:
            self.previous.update_sizes(value)

    def __str__(self):
        return self.name + " " + self.type + " " + str(self.size)


fake_tree = Node('/', 'dir')
wd = fake_tree

for line in commands:
    c = line[0]
    output = line[2:]

    # handle changing directory
    if c == '/':
        wd = fake_tree
    elif c == '..':
        wd = wd.previous
    else:
        for n in wd.next_nodes:
            if c == n.name:
                wd = n

    # populate data from ls
    if output:
        for o in output:
            next_node_names = [n.name for n in wd.next_nodes]
            if o not in next_node_names:
                    value, name = o.split()
                    wd.add_node(name, value)
                


def print_fake_tree(tree, indent = ''):
    # print(len(tree.next_nodes))
    print(indent + str(tree))
    for node in tree.next_nodes:
        print_fake_tree(node,indent + '   ')

total = 0
def get_part1(tree):
    if tree.size <= 100000 and tree.type == 'dir':
        global total
        total += tree.size
    for node in tree.next_nodes:
        get_part1(node)


print_fake_tree(fake_tree)

get_part1(fake_tree)
print(total)

# Part 2

total_space = 70000000
needed_space = 30000000
to_free_space = needed_space - (total_space - fake_tree.size)

smallest = total_space
def get_part2(tree):
    global smallest
    # print(tree.size)
    if tree.size <= smallest and tree.size >= to_free_space and tree.type == 'dir':
        smallest = tree.size
    for node in tree.next_nodes:
        get_part2(node)


get_part2(fake_tree)
print(smallest)