#! /usr/bin/env python3
import input_data
data = input_data.day3_data

letters= list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')

# Part 1
total = 0
for line in data.splitlines():
    size = len(line)//2
    c1 = set(line[:size])
    c2 = set(line[size:])
    common = c1.intersection(c2).pop()
    value = letters.index(str(common))+1
    total += value


print(total)

# Part 2
total = 0
lines = data.splitlines()

for i in range(len(lines)):
    if i % 3 == 0:
        r1 = set(lines[i])
        r2 = set(lines[i+1])
        r3 = set(lines[i+2])
        common = r1.intersection(r2).intersection(r3).pop()
        value = letters.index(str(common))+1
        total += value
    else:
        continue


print(total)