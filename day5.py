#!/usr/bin/env python3
import input_data
import re
data = input_data.day5_data


stacks = """                [B]     [L]     [S]
        [Q] [J] [C]     [W]     [F]
    [F] [T] [B] [D]     [P]     [P]
    [S] [J] [Z] [T]     [B] [C] [H]
    [L] [H] [H] [Z] [G] [Z] [G] [R]
[R] [H] [D] [R] [F] [C] [V] [Q] [T]
[C] [J] [M] [G] [P] [H] [N] [J] [D]
[H] [B] [R] [S] [R] [T] [S] [R] [L]
 1   2   3   4   5   6   7   8   9 """

rows = stacks.splitlines()
moves = data.splitlines()
moves = [re.findall('\d+',m) for m in moves]

locations = str(rows.pop())

def reset_columns():
    columns = []
    for i in range(0,9):
        columns.append([])
        x = locations.index(f"{i+1}")
        for r in rows:
            if r[x] != ' ':
                columns[i].append(r[x])
    return columns

# Part 1
columns = reset_columns()

for m in moves:
    for i in range(int(m[0])):
        start = int(m[1])-1
        end = int(m[2])-1
        columns[end].insert(0, columns[start].pop(0))

answer = ''
for c in columns:
    answer += c[0]
print(answer)

# Part 2
columns = reset_columns()

for m in moves:
    count = int(m[0])
    holding = []
    for i in range(count):
        start = int(m[1])-1
        end = int(m[2])-1
        holding.append(columns[start].pop(0))
    columns[end] = holding + columns[end]

answer = ''
for c in columns:
    answer += c[0]
print(answer)