#!/usr/bin/env python3
import input_data
data = input_data.day4_data

split_pairs = [p.split(',') for p in data.splitlines()]

# Part 1
overlapping = 0

for e1, e2 in split_pairs:
    e1 = e1.split('-')
    e2 = e2.split('-')
    e1 = [int(e) for e in e1]
    e2= [int(e) for e in e2]

    if(e1[0] >= e2[0] and e1[1] <= e2[1]):
        overlapping+=1
    elif(e2[0] >= e1[0] and e2[1] <= e1[1]):
        overlapping+=1

print(overlapping)


# Part 2
overlapping = 0

for e1, e2 in split_pairs:
    e1 = e1.split('-')
    e2 = e2.split('-')
    e1 = [int(e) for e in e1]
    e2= [int(e) for e in e2]

    if(e1[0] >= e2[0] and e1[1] <= e2[1]):
        overlapping+=1
    elif(e2[0] >= e1[0] and e2[1] <= e1[1]):
        overlapping+=1
    elif(e1[0] >= e2[0] and e1[0] <= e2[1]):
        overlapping+=1
    elif(e2[0] >= e1[0] and e2[0] <= e1[1]):
        overlapping+=1

print(overlapping)