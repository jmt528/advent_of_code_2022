#!/usr/bin/env python3
import input_data
data = input_data.day2_data

totalscore = 0
for line in data.splitlines():
    values = line.split()
    if values[1] == 'X':
        totalscore += 1
        if values[0] == 'C':
            totalscore += 6
        elif values[0] == 'A':
            totalscore += 3
    if values[1] == 'Y':
        totalscore += 2
        if values[0] == 'A':
            totalscore += 6
        elif values[0] == 'B':
            totalscore += 3
    if values[1] == 'Z':
        totalscore += 3
        if values[0] == 'B':
            totalscore += 6
        elif values[0] == 'C':
            totalscore += 3

print(totalscore)

totalscore = 0

for line in data.splitlines():
    values = line.split()
    if values[1] == 'X':
        totalscore += 0
        if values[0] == 'B':
            totalscore += 1
        elif values[0] == 'C':
            totalscore += 2
        elif values[0] == 'A':
            totalscore += 3
    if values[1] == 'Y':
        totalscore += 3
        if values[0] == 'A':
            totalscore += 1
        elif values[0] == 'B':
            totalscore += 2
        elif values[0] == 'C':
            totalscore += 3
    if values[1] == 'Z':
        totalscore += 6
        if values[0] == 'C':
            totalscore += 1
        elif values[0] == 'A':
            totalscore += 2
        elif values[0] == 'B':
            totalscore += 3

print(totalscore)
