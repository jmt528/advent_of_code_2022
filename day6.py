#!/usr/bin/env python3
import input_data
data = input_data.day6_data

l = list(data)

# Part 1
for i in range(len(l)):
    s = set(l[i:i+4])
    if len(s) == 4:
        print(i+4)
        break

# Part 2
for i in range(len(l)):
    s = set(l[i:i+14])
    if len(s) == 14:
        print(i+14)
        break