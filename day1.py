#! /usr/bin/env python3
import input_data

data = input_data.day1_data
total = 0
biggest_val = 0
biggest_three = [0,0,0]

for line in data.splitlines():
    if line != '':
        total += int(line)
    else:
        if total > biggest_val:
            biggest_val = total
        if total > biggest_three[2]:
            biggest_three.pop()
            biggest_three.append(total)
            biggest_three = sorted(biggest_three, reverse=True)
        total = 0
        
print(f"The answer to part 1 is: {biggest_val}")
print(f"The answer to part 2 is: {sum(biggest_three)}")
    